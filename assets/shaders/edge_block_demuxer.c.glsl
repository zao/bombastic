#version 450 core

layout(r8ui) uniform restrict readonly uimage2DMS lv_MaterialImage;

layout(local_size_x = 1, local_size_y = 1, local_size_z = 1) in;

layout(std430, binding=0) buffer Edges {
    uint x_count, y_count, z_count;
    uint indices[];
} edges;

layout(std430, binding=1) buffer Interiors {
    uint x_count, y_count, z_count;
    uint indices[];
} interiors;

void record_edge_tile() {
    uint slot = atomicAdd(edges.x_count, 1);
    uint index = gl_NumWorkGroups.x * gl_WorkGroupID.y + gl_WorkGroupID.x;
    edges.indices[slot] = index;
}

void record_interior_tile() {
    uint slot = atomicAdd(interiors.x_count, 1);
    uint index = gl_NumWorkGroups.x * gl_WorkGroupID.y + gl_WorkGroupID.x;
    interiors.indices[slot] = index;
}

void main() {
    ivec2 tc_base = ivec2(gl_WorkGroupID.xy) * ivec2(8, 8);
    int sample_count = imageSamples(lv_MaterialImage);
    uint root_id = imageLoad(lv_MaterialImage, tc_base, 0).r;
    for (int row = 0; row < 8; ++row) {
        for (int col = 0; col < 8; ++col) {
            ivec2 tc = tc_base + ivec2(col, row);
            for (int smp = 0; smp < sample_count; ++smp) {
                uint id = imageLoad(lv_MaterialImage, tc, smp).r;
                if (root_id != id) {
                    record_edge_tile();
                    return;
                }
            }
        }
    }
    record_interior_tile();
}
