#version 450 core

in vec3 lv_WorldNormal;
in vec4 lv_TangentFrameQuat;
in vec2 lv_Texcoord;
in vec3 lv_ViewPosition;
flat in uint lv_MaterialID;

layout(location=0) out uvec4 lv_TangentFrameOut;
layout(location=1) out vec2 lv_TextureCoordinateOut;
layout(location=2) out vec2 lv_DepthGradientOut;
layout(location=3) out vec4 lv_UVGradientOut;
layout(location=4) out uint lv_MaterialIDOut;

uint quantize_value(float v) {
	v *= sqrt(2.0);
	v += 1.0;
	v /= 2.0;
	v *= 1023.0;
	return clamp(uint(v), 0, 1023);
}

uvec4 compress_quat(vec4 q) {
	vec4 mag = abs(q);
	if (mag.x >= mag.y && mag.x >= mag.z && mag.x >= mag.w) {
		if (q.x < 0.0) q = -q;
		return uvec4(quantize_value(q.y), quantize_value(q.z), quantize_value(q.w), 0);
	}
	if (mag.y >= mag.x && mag.y >= mag.z && mag.y >= mag.w) {
		if (q.y < 0.0) q = -q;
		return uvec4(quantize_value(q.x), quantize_value(q.z), quantize_value(q.w), 1);
	}
	if (mag.z >= mag.x && mag.z >= mag.y && mag.z >= mag.w) {
		if (q.z < 0.0) q = -q;
		return uvec4(quantize_value(q.x), quantize_value(q.y), quantize_value(q.w), 2);
	}
	if (q.w < 0.0) q = -q;
	return uvec4(quantize_value(q.x), quantize_value(q.y), quantize_value(q.z), 3);
}

void main() {
	uvec4 comp_q = compress_quat(lv_TangentFrameQuat);
	lv_TangentFrameOut = comp_q;
	lv_TextureCoordinateOut = lv_Texcoord;
	lv_DepthGradientOut = vec2(dFdx(lv_ViewPosition.z), dFdy(lv_ViewPosition.z));
	lv_UVGradientOut = vec4(dFdx(lv_Texcoord.x), dFdy(lv_Texcoord.x), dFdx(lv_Texcoord.y), dFdy(lv_Texcoord.y));
	lv_MaterialIDOut = lv_MaterialID;
}
