#version 450 core

layout(binding=0) uniform samplerCube lv_Cubemap;

layout(location=0) in vec3 lv_Direction;

layout(location=0) out vec4 lv_DiffuseColor;

void main() {
	vec3 color = texture(lv_Cubemap, lv_Direction).rgb;
	lv_DiffuseColor = vec4(color, 1.0f);
}
