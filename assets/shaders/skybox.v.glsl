#version 450 core

in int gl_VertexID;

uniform mat4x4 lv_ProjectionMatrix;
uniform mat4x4 lv_ViewMatrix;

struct Vertex {
    vec4 pos;
};

layout(std430, binding=0) buffer Vertices {
    Vertex vertices[];
};

layout(location=0) out vec3 lv_Direction;

void main() {
    vec4 pos = vertices[gl_VertexID].pos;
	lv_Direction = pos.xyz * vec3(-1.0, 1.0, 1.0);
	vec4 ndc = lv_ProjectionMatrix * lv_ViewMatrix * vec4(pos.xyz, 0.0);
    gl_Position = ndc.xyww;
}
