#version 450 core

in int gl_VertexID;
in int gl_InstanceID;

uniform mat4x4 lv_ProjectionMatrix;
uniform mat4x4 lv_ViewMatrix;

struct Vertex {
    vec4 pos;
    vec3 normal; float _pad0;
    vec2 tc; vec2 _pad1;
	vec3 tangent_u; float _pad2;
	vec3 tangent_v; float _pad3;
};

layout(std430, binding=0) buffer Vertices {
    Vertex vertices[];
};

struct Instance {
    mat4 world;
    uint material_id;
};

layout(std430, binding=1) buffer Instances {
    Instance instances[];
};

out vec3 lv_WorldNormal;
out vec4 lv_TangentFrameQuat;
out vec2 lv_Texcoord;
out vec3 lv_ViewPosition;
out uint lv_MaterialID;

float copysign(float x, float y) {
	return (y < 0.0) ? -1.0 : 1.0;
}

vec4 mat3_to_quat(mat3 m) {
	float t = m[0][0] + m[1][1] + m[2][2];
	float r = sqrt(1.0 + t);
	float w = 0.5 * r;
	float x = copysign(0.5 * sqrt(1.0 + m[0][0] - m[1][1] - m[2][2]), m[1][2] - m[2][1]);
	float y = copysign(0.5 * sqrt(1.0 - m[0][0] + m[1][1] - m[2][2]), m[2][0] - m[0][2]);
	float z = copysign(0.5 * sqrt(1.0 - m[0][0] - m[1][1] + m[2][2]), m[0][1] - m[1][0]);
	return vec4(x, y, z, w);
}

void main() {
    Vertex vtx = vertices[gl_VertexID];
    Instance inst = instances[gl_InstanceID];
    mat4 world = inst.world;
    vec4 viewPosition = lv_ViewMatrix * world * vtx.pos;
    lv_ViewPosition = viewPosition.xyz;
	vec4 ndc = lv_ProjectionMatrix * viewPosition;
    gl_Position = ndc;
    lv_WorldNormal = vtx.normal;
    mat3 normalMatrix = mat3(lv_ViewMatrix) * mat3(world);
    lv_TangentFrameQuat = mat3_to_quat(mat3(
        normalMatrix * vtx.normal,
        normalMatrix * vtx.tangent_u, // yes, not quite right, but works out
        normalMatrix * vtx.tangent_v));
    lv_Texcoord = vtx.tc;
    lv_MaterialID = inst.material_id;
}
