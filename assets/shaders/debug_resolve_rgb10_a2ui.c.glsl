#version 450 core

layout(rgb10_a2ui) uniform restrict readonly uimage2DMS lv_SourceImage;
uniform restrict writeonly image2D lv_TargetImage;

layout(local_size_x = 1, local_size_y = 1, local_size_z = 1) in;

void main() {
    int sample_count = imageSamples(lv_SourceImage);
    for (int row = 0; row < 8; ++row) {
        for (int col = 0; col < 8; ++col) {
            ivec2 tc = ivec2(gl_WorkGroupID.xy) * ivec2(8, 8) + ivec2(col, row);
            vec4 I = vec4(0.0);
            for (int smp = 0; smp < sample_count; ++smp) {
                uvec4 v = imageLoad(lv_SourceImage, tc, smp);
                I += vec4(v) / vec4(1023.0, 1023.0, 1023.0, 3.0);
            }
            I /= sample_count;
            imageStore(lv_TargetImage, tc, I);
        }
    }
}
