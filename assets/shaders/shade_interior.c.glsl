#version 450 core

layout(rgb10_a2ui) uniform restrict readonly uimage2DMS lv_TangentFrameImage;
layout(rg16f) uniform restrict readonly image2DMS lv_TextureCoordinateImage;
layout(rg16f) uniform restrict readonly image2DMS lv_DepthGradientImage;
layout(rgba16f) uniform restrict readonly image2DMS lv_UvGradientImage;
layout(r8ui) uniform restrict readonly uimage2DMS lv_MaterialImage;

layout(rgba8) uniform restrict writeonly image2DMS lv_TargetImage;

uniform sampler2D lv_DiffuseTexture;

layout(local_size_x = 8, local_size_y = 8, local_size_z = 1) in;

layout(std430, binding=0) readonly buffer Tiles {
    uint x_count, y_count, z_count;
    uint indices[];
} tiles;

void main() {
    ivec2 tile_counts = imageSize(lv_MaterialImage) / ivec2(8, 8);
    uint tile_id = tiles.indices[gl_WorkGroupID.x];
    int tile_x = int(tile_id % tile_counts.x);
    int tile_y = int(tile_id / tile_counts.x);
    ivec2 tc_base = ivec2(tile_x, tile_y) * ivec2(8, 8);
    int sample_count = imageSamples(lv_MaterialImage);
    int col = int(gl_LocalInvocationID.x);
    int row = int(gl_LocalInvocationID.y);
    ivec2 tc = tc_base + ivec2(col, row);
    uint mat_id = imageLoad(lv_MaterialImage, tc, 0).r;
    if (mat_id != 0) {
        vec2 shade_tc = imageLoad(lv_TextureCoordinateImage, tc, 0).rg;
        vec4 uv_grad = imageLoad(lv_UvGradientImage, tc, 0);
        vec4 color = textureLod(lv_DiffuseTexture, shade_tc, 4);
        color = textureGrad(lv_DiffuseTexture, shade_tc, uv_grad.xz, uv_grad.yw);
        for (int smp = 0; smp < sample_count; ++smp) {
            imageStore(lv_TargetImage, tc, smp, color);
        }
    }
}
