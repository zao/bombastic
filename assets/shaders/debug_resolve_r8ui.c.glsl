#version 450 core

layout(r8ui) uniform restrict readonly uimage2DMS lv_SourceImage;
uniform restrict writeonly image2D lv_TargetImage;

layout(local_size_x = 1, local_size_y = 1, local_size_z = 1) in;

void main() {
    int sample_count = imageSamples(lv_SourceImage);
    for (int row = 0; row < 8; ++row) {
        for (int col = 0; col < 8; ++col) {
            ivec2 tc = ivec2(gl_WorkGroupID.xy) * ivec2(8, 8) + ivec2(col, row);
            float I = 0.0;
            for (int smp = 0; smp < sample_count; ++smp) {
                uint id = imageLoad(lv_SourceImage, tc, smp).r;
                I += float(id) / 255.0;
            }
            I /= sample_count;
            imageStore(lv_TargetImage, tc, vec4(I, I, I, 1.0));
        }
    }
}
