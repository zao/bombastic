#include <GLXW/glxw.h>
#include <GLFW/glfw3.h>

#include <algorithm>
#include <atomic>
#include <utility>

#include "MathGeoLib.h"
#include "stb_image.h"

#include <sys/stat.h>

GLFWwindow* g_window;
std::atomic<bool> g_keep_running{ true };
GLint g_width{1};
GLint g_height{1};
float g_aspect{1.0f};
GLint g_sample_count{4};
GLuint g_vao{};

int debug_perspective = 0;

void on_glfw_key(GLFWwindow* window, int key, int scancode, int action, int mods) {
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
		glfwSetWindowShouldClose(g_window, GL_TRUE);
	}
	if (key == GLFW_KEY_1 && action == GLFW_RELEASE) {
		debug_perspective = 1;
	}
	if (key == GLFW_KEY_2 && action == GLFW_RELEASE) {
		debug_perspective = 2;
	}
	if (key == GLFW_KEY_3 && action == GLFW_RELEASE) {
		debug_perspective = 3;
	}
	if (key == GLFW_KEY_4 && action == GLFW_RELEASE) {
		debug_perspective = 4;
	}
	if (key == GLFW_KEY_5 && action == GLFW_RELEASE) {
		debug_perspective = 5;
	}
	if (key == GLFW_KEY_6 && action == GLFW_RELEASE) {
		debug_perspective = 0;
	}
}

std::string fs_root;

void fs_init() {
	char buf[1 << 16];
	size_t end = GetModuleFileNameA(nullptr, buf, _countof(buf));
	while (end) {
		if (buf[end] == '\\' || buf[end] == '/') {
			buf[end] = '\0';
			strcat(buf + end, "\\assets");
			struct stat s;
			if (0 == stat(buf, &s) && (s.st_mode & _S_IFDIR)) {
				fs_root = std::string(buf, buf + end);
				return;
			}
		}
		--end;
	}
}

void APIENTRY on_gl_error(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, GLchar const* message, void const* param) {
	static char buf[1<<16];
	if (severity != GL_DEBUG_SEVERITY_NOTIFICATION) {
		sprintf(buf, "GL_DEBUG: %d %d %d %d %d \"%s\"\n", source, type, id, severity, length, message);
		OutputDebugStringA(buf);
		fprintf(stderr, "%s", buf);
	}
}

void vid_init() {
	bool debug_context = true;
	glfwInit();
	using Hint = std::pair<int, int>;
	Hint hints[] = {
		{ GLFW_RESIZABLE, GL_FALSE },
		{ GLFW_VISIBLE, GL_TRUE },
		{ GLFW_DECORATED, GL_TRUE },
		{ GLFW_FOCUSED, GL_FALSE },
		{ GLFW_DEPTH_BITS, 0 },
		{ GLFW_CLIENT_API, GLFW_OPENGL_API },
		{ GLFW_SAMPLES, 1 },
		{ GLFW_DOUBLEBUFFER, GL_TRUE },
		{ GLFW_CONTEXT_VERSION_MAJOR, 4 },
		{ GLFW_CONTEXT_VERSION_MINOR, 5 },
		{ GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE },
		{ GLFW_OPENGL_DEBUG_CONTEXT, debug_context ? GL_TRUE : GL_FALSE },
		{ GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE },
	};
	for (auto hint : hints) {
		glfwWindowHint(hint.first, hint.second);
	}
	bool fullscreen = false;
	if (fullscreen) {
		GLFWmonitor* mon = glfwGetPrimaryMonitor();
		GLFWvidmode const* vm = glfwGetVideoMode(mon);
		g_width = vm->width;
		g_height = vm->height;
		g_window = glfwCreateWindow(g_width, g_height, "untitled", glfwGetPrimaryMonitor(), nullptr);
	}
	else {
		g_width = 1920;
		g_height = 1080;
		g_window = glfwCreateWindow(g_width, g_height, "untitled", nullptr, nullptr);
	}
	g_aspect = static_cast<float>(g_width) / static_cast<float>(g_height);
	glfwMakeContextCurrent(g_window);
	glxwInit();
	if (debug_context) {
		glDebugMessageCallback(on_gl_error, nullptr);
	}
	glfwSetKeyCallback(g_window, on_glfw_key);
	glCreateVertexArrays(1, &g_vao);
	glBindVertexArray(g_vao);
	while (glGetError()) {}
	glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
	glEnable(GL_CULL_FACE);
}

void vid_terminate() {
	glfwDestroyWindow(g_window);
	g_window = nullptr;
	glfwTerminate();
}

GLuint screen_fbo{};
struct gbuffer {
	enum MSAATextureID {
		depth_texture,
		tangent_frame_texture,
		texture_coordinate_texture,
		depth_gradient_texture,
		uv_gradient_texture,
		material_id_texture,
		pre_resolve_texture,
		msaa_texture_count,
	};
	GLuint msaa_ids[msaa_texture_count];

	enum RegularTextureID {
		post_resolve_texture,
		regular_texture_count,
	};
	GLuint tex_ids[regular_texture_count];

	enum FramebufferID {
		geometry_draw_fbo,
		sky_draw_fbo,
		resolve_fbo,
		fbo_count,
	};
	GLuint fbo_ids[fbo_count];
};

static gbuffer gbuf;

struct cubemap {
	GLuint tex_id;
};

cubemap clouds;

struct render_shaders {
	GLuint skybox_prg{};
	GLuint solid_prg{};
	GLuint debug_resolve_rgb10_a2ui_prg{};
	GLuint debug_resolve_r8ui_prg{};
	GLuint edge_classify_prg{};
	GLuint shade_edge_prg{};
	GLuint shade_interior_prg{};
};

render_shaders shaders;

struct skybox_geometry {
	GLuint vertices{};
	GLuint indices{};
	GLuint index_count{};
};

skybox_geometry skybox_data;

struct solid_geometry {
	GLuint vertices{};
	GLuint indices{};
	GLuint index_count{};
};

solid_geometry cube;

struct terrain_geometry {
	GLuint vertices{};
	GLuint indices{};
	GLuint index_count;
};

terrain_geometry terrain;

struct debug_textures {
	GLuint uv{};
};

debug_textures dbg_tex;

std::unique_ptr<std::vector<uint8_t>> slurp_file(char const* file) {
	std::string filename = fs_root + "\\assets\\shaders\\" + file;
	FILE* fh = fopen(filename.c_str(), "rb");
	if (!fh) return {};
	fseek(fh, 0, SEEK_END);
	size_t cb = ftell(fh);
	fseek(fh, 0, SEEK_SET);
	auto ret = std::make_unique<std::vector<uint8_t>>(cb);
	fread(ret->data(), ret->size(), 1, fh);
	fclose(fh);
	return ret;
}

GLuint build_program(char const* vs_filename, char const* fs_filename) {
	GLenum err{ GL_NO_ERROR };
	char* log_buf{};
	GLsizei log_len{};
	err = glGetError();

	GLuint vs_id;
	GLuint fs_id;
	GLuint prg;
	vs_id = glCreateShader(GL_VERTEX_SHADER);
	fs_id = glCreateShader(GL_FRAGMENT_SHADER);
	prg = glCreateProgram();

	while (true) {
		bool all_good = true;
		auto vs_source = slurp_file(vs_filename);
		if (!vs_source) {
			std::cerr << "VS (" << vs_filename << "): not found\n";
			all_good = false;
		}
		else {
			GLchar const* p[] = { const_cast<GLchar const*>(reinterpret_cast<GLchar*>(vs_source->data())) };
			GLint lengths[] = { static_cast<GLint>(vs_source->size()) };
			glShaderSource(vs_id, 1, p, lengths);
			err = glGetError();
			glCompileShader(vs_id);
			GLint status{};
			glGetShaderiv(vs_id, GL_COMPILE_STATUS, &status);
			glGetShaderiv(vs_id, GL_INFO_LOG_LENGTH, &log_len);
			err = glGetError();
			log_buf = (char*)realloc(log_buf, log_len + 1);
			glGetShaderInfoLog(vs_id, log_len, &log_len, log_buf);
			log_buf[log_len] = '\0';
			if (status != GL_TRUE) {
				std::cerr << "VS (" << vs_filename << "):\n" << std::string(log_buf, log_buf + log_len) << std::endl;
				all_good = false;
			}
		}

		auto fs_source = slurp_file(fs_filename);
		if (!fs_source) {
			std::cerr << "FS (" << fs_filename << "): not found\n";
			all_good = false;
		}
		else {
			GLchar const* p[] = { const_cast<GLchar const*>(reinterpret_cast<GLchar*>(fs_source->data())) };
			GLint lengths[] = { static_cast<GLint>(fs_source->size()) };
			glShaderSource(fs_id, 1, p, lengths);
			err = glGetError();
			glCompileShader(fs_id);
			err = glGetError();
			GLint status{};
			glGetShaderiv(fs_id, GL_COMPILE_STATUS, &status);
			glGetShaderiv(fs_id, GL_INFO_LOG_LENGTH, &log_len);
			log_buf = (char*)realloc(log_buf, log_len + 1);
			glGetShaderInfoLog(fs_id, log_len, &log_len, log_buf);
			log_buf[log_len] = '\0';
			if (status != GL_TRUE) {
				std::cerr << "FS (" << fs_filename << "):\n" << std::string(log_buf, log_buf + log_len) << std::endl;
				all_good = false;
			}
		}

		glAttachShader(prg, vs_id);
		glAttachShader(prg, fs_id);
		glLinkProgram(prg);
		err = glGetError();
		GLint status{};
		glGetProgramiv(prg, GL_LINK_STATUS, &status);
		glGetProgramiv(prg, GL_INFO_LOG_LENGTH, &log_len);
		log_buf = (char*)realloc(log_buf, log_len + 1);
		glGetProgramInfoLog(prg, log_len, &log_len, log_buf);
		log_buf[log_len] = '\0';
		if (status != GL_TRUE) {
			std::cerr << "Program " << "(" << vs_filename << "|" << fs_filename << "):\n" << std::string(log_buf, log_buf + log_len) << std::endl;
			all_good = false;
		}
		glDetachShader(prg, vs_id);
		glDetachShader(prg, fs_id);
		err = glGetError();
		if (all_good) {
			glDeleteShader(vs_id);
			glDeleteShader(fs_id);
			break;
		}
		Sleep(1000);
	}
	free(log_buf);
	return prg;
}

GLuint build_program(char const* cs_filename) {
	GLenum err{ GL_NO_ERROR };
	char* log_buf{};
	GLsizei log_len{};
	err = glGetError();

	GLuint cs_id;
	GLuint prg;
	cs_id = glCreateShader(GL_COMPUTE_SHADER);
	prg = glCreateProgram();

	while (true) {
		bool all_good = true;
		auto cs_source = slurp_file(cs_filename);
		if (!cs_source) {
			std::cerr << "CS (" << cs_filename << "): not found\n";
			all_good = false;
		}
		else {
			GLchar const* p[] = { const_cast<GLchar const*>(reinterpret_cast<GLchar*>(cs_source->data())) };
			GLint lengths[] = { static_cast<GLint>(cs_source->size()) };
			glShaderSource(cs_id, 1, p, lengths);
			err = glGetError();
			glCompileShader(cs_id);
			GLint status{};
			glGetShaderiv(cs_id, GL_COMPILE_STATUS, &status);
			glGetShaderiv(cs_id, GL_INFO_LOG_LENGTH, &log_len);
			err = glGetError();
			log_buf = (char*)realloc(log_buf, log_len + 1);
			glGetShaderInfoLog(cs_id, log_len, &log_len, log_buf);
			log_buf[log_len] = '\0';
			if (status != GL_TRUE) {
				std::cerr << "CS (" << cs_filename << "):\n" << std::string(log_buf, log_buf + log_len) << std::endl;
				all_good = false;
			}
		}

		glAttachShader(prg, cs_id);
		glLinkProgram(prg);
		err = glGetError();
		GLint status{};
		glGetProgramiv(prg, GL_LINK_STATUS, &status);
		glGetProgramiv(prg, GL_INFO_LOG_LENGTH, &log_len);
		log_buf = (char*)realloc(log_buf, log_len + 1);
		glGetProgramInfoLog(prg, log_len, &log_len, log_buf);
		log_buf[log_len] = '\0';
		if (status != GL_TRUE) {
			std::cerr << "Program " << "(" << cs_filename << "):\n" << std::string(log_buf, log_buf + log_len) << std::endl;
			all_good = false;
		}
		glDetachShader(prg, cs_id);
		err = glGetError();
		if (all_good) {
			glDeleteShader(cs_id);
			break;
		}
		Sleep(1000);
	}
	free(log_buf);
	return prg;
}

void shader_init() {
	shaders.skybox_prg = build_program("skybox.v.glsl", "skybox.f.glsl");
	shaders.solid_prg = build_program("solid.v.glsl", "solid.f.glsl");
	shaders.debug_resolve_rgb10_a2ui_prg = build_program("debug_resolve_rgb10_a2ui.c.glsl");
	shaders.debug_resolve_r8ui_prg = build_program("debug_resolve_r8ui.c.glsl");
	shaders.edge_classify_prg = build_program("edge_block_demuxer.c.glsl");
	shaders.shade_edge_prg = build_program("shade_edge.c.glsl");
	shaders.shade_interior_prg = build_program("shade_interior.c.glsl");
}

void geometry_init() {
	struct SolidVertex {
		float4 pos;
		float3 normal; float _pad0;
		float2 tc; float2 _pad1;
		float3 tangent_u; float _pad2;
		float3 tangent_v; float _pad3;
	};
	struct Vertex {
		float4 pos;
	};
	Vertex corners[] = {
		{ float4(-1.0f, -1.0f, -1.0f, 1.0f) }, //    2-------3
		{ float4(+1.0f, -1.0f, -1.0f, 1.0f) }, //   /|      /|
		{ float4(-1.0f, +1.0f, -1.0f, 1.0f) }, //  6-------7 |
		{ float4(+1.0f, +1.0f, -1.0f, 1.0f) }, //  | |     | |
		{ float4(-1.0f, -1.0f, +1.0f, 1.0f) }, //  | 0-----|-1
		{ float4(+1.0f, -1.0f, +1.0f, 1.0f) }, //  |/      |/
		{ float4(-1.0f, +1.0f, +1.0f, 1.0f) }, //  4-------5
		{ float4(+1.0f, +1.0f, +1.0f, 1.0f) }, // 
	};
	float3 normals[] = {
		-float3::unitX, +float3::unitX,
		-float3::unitY, +float3::unitY,
		-float3::unitZ, +float3::unitZ,
	};
	float3 tangents_u[] = {
		+float3::unitZ, -float3::unitZ,
		-float3::unitZ, +float3::unitZ,
		+float3::unitY, +float3::unitY,
	};
	float3 tangents_v[] = {
		+float3::unitY, +float3::unitY,
		+float3::unitX, +float3::unitX,
		+float3::unitX, -float3::unitX,
	};
	float2 tcs[] = {
		float2(0.0f, 1.0f),
		float2(1.0f, 1.0f),
		float2(0.0f, 0.0f),
		float2(1.0f, 0.0f),
	};
	uint16_t indices[] = {
		0, 1, 2,
		2, 1, 3,
		4, 0, 6,
		6, 0, 2,
		5, 4, 7,
		7, 4, 6,
		1, 5, 3,
		3, 5, 7,
		2, 3, 6,
		6, 3, 7,
		4, 5, 0,
		0, 5, 1,
	};
	auto& vb = skybox_data.vertices;
	auto& ib = skybox_data.indices;
	glCreateBuffers(1, &vb);
	glNamedBufferStorage(vb, sizeof(corners), corners, 0);
	glCreateBuffers(1, &ib);
	glNamedBufferStorage(ib, sizeof(indices), indices, 0);
	skybox_data.index_count = _countof(indices);
	struct face {
		int ix[4];
	};
	face faces[] = {
		{ 4, 6, 0, 2 }, // -X
		{ 1, 3, 5, 7 }, // +X
		{ 4, 0, 5, 1 }, // -Y
		{ 2, 6, 3, 7 }, // +Y
		{ 0, 2, 1, 3 }, // -Z
		{ 5, 7, 4, 6 }, // +Z
	};
	for (size_t i = 0; i < _countof(indices); i += 3) {
		std::swap(indices[i+1], indices[i+2]);
	}
	std::vector<SolidVertex> solid_vertices;
	solid_vertices.reserve(6 * 4);
	std::vector<uint16_t> solid_indices;
	solid_indices.reserve(6 * 6);
	for (size_t face_idx = 0; face_idx < 6; ++face_idx) {
		uint16_t base_idx = static_cast<uint16_t>(solid_vertices.size());
		auto& f = faces[face_idx].ix;
		for (size_t i = 0; i < 4; ++i) {
			SolidVertex vtx;
			vtx.pos = corners[f[i]].pos;
			vtx.normal = normals[face_idx];
			vtx.tangent_u = tangents_u[face_idx];
			vtx.tangent_v = tangents_v[face_idx];
			vtx.tc = tcs[i];
			solid_vertices.push_back(vtx);
		}
		int idx_offsets[] = { 0, 1, 2, 2, 1, 3 };
		for (auto off : idx_offsets) {
			solid_indices.push_back(base_idx + off);
		}
	}
	glCreateBuffers(1, &cube.vertices);
	glNamedBufferStorage(cube.vertices, sizeof(SolidVertex) * solid_vertices.size(), solid_vertices.data(), 0);
	glCreateBuffers(1, &cube.indices);
	glNamedBufferStorage(cube.indices, sizeof(uint16_t) * solid_indices.size(), solid_indices.data(), 0);
	cube.index_count = static_cast<GLuint>(solid_indices.size());

	{
		float3 n = +float3::unitY;
		float3 t = +float3::unitZ;
		float3 b = +float3::unitX;
		SolidVertex verts[] = {
			{ float4(-1.0f, 0.0f, -1.0f, 1.0f), n, {}, float2(0.0f, 0.0f), {}, t, {}, b, {}, }, //   0-----1
			{ float4(+1.0f, 0.0f, -1.0f, 1.0f), n, {}, float2(1.0f, 0.0f), {}, t, {}, b, {}, }, //  /     /
			{ float4(-1.0f, 0.0f, +1.0f, 1.0f), n, {}, float2(0.0f, 1.0f), {}, t, {}, b, {}, }, // 2-----3
			{ float4(+1.0f, 0.0f, +1.0f, 1.0f), n, {}, float2(1.0f, 1.0f), {}, t, {}, b, {}, }, //
		};
		uint16_t indices[] = {
			0, 2, 1, 1, 2, 3,
		};
		union {
			struct { GLuint vb, ib; };
			GLuint ids[2];
		} bufs;
		glCreateBuffers(2, bufs.ids);
		glNamedBufferStorage(bufs.vb, sizeof(verts), verts, 0);
		glNamedBufferStorage(bufs.ib, sizeof(indices), indices, 0);
		terrain.vertices = bufs.vb;
		terrain.indices = bufs.ib;
		terrain.index_count = _countof(indices);
	}
}

void init_gbuffer() {
	GLenum err{ GL_NO_ERROR };
	err = glGetError();
	GLenum status{};
	glCreateTextures(GL_TEXTURE_2D_MULTISAMPLE, gbuffer::msaa_texture_count, gbuf.msaa_ids);
	glCreateTextures(GL_TEXTURE_2D, gbuffer::regular_texture_count, gbuf.tex_ids);
	glTextureStorage2DMultisample(gbuf.msaa_ids[gbuffer::depth_texture], g_sample_count,
		GL_DEPTH24_STENCIL8, g_width, g_height, GL_TRUE);
	glTextureStorage2DMultisample(gbuf.msaa_ids[gbuffer::tangent_frame_texture], g_sample_count,
		GL_RGB10_A2UI, g_width, g_height, GL_TRUE);
	glTextureStorage2DMultisample(gbuf.msaa_ids[gbuffer::texture_coordinate_texture], g_sample_count,
		GL_RG16F, g_width, g_height, GL_TRUE);
	glTextureStorage2DMultisample(gbuf.msaa_ids[gbuffer::depth_gradient_texture], g_sample_count,
		GL_RG16F, g_width, g_height, GL_TRUE);
	glTextureStorage2DMultisample(gbuf.msaa_ids[gbuffer::uv_gradient_texture], g_sample_count,
		GL_RGBA16F, g_width, g_height, GL_TRUE);
	glTextureStorage2DMultisample(gbuf.msaa_ids[gbuffer::material_id_texture], g_sample_count,
		GL_R8UI, g_width, g_height, GL_TRUE);
	glTextureStorage2DMultisample(gbuf.msaa_ids[gbuffer::pre_resolve_texture], g_sample_count,
		GL_RGBA8, g_width, g_height, GL_TRUE);
	glTextureStorage2D(gbuf.tex_ids[gbuffer::post_resolve_texture], 1,
		GL_RGBA8, g_width, g_height);
	err = glGetError();

	glCreateFramebuffers(gbuffer::fbo_count, gbuf.fbo_ids);
	auto fbo = gbuf.fbo_ids[gbuffer::geometry_draw_fbo];
	glNamedFramebufferTexture(fbo, GL_DEPTH_ATTACHMENT, gbuf.msaa_ids[gbuffer::depth_texture], 0);
	glNamedFramebufferTexture(fbo, GL_COLOR_ATTACHMENT0, gbuf.msaa_ids[gbuffer::tangent_frame_texture], 0);
	glNamedFramebufferTexture(fbo, GL_COLOR_ATTACHMENT1, gbuf.msaa_ids[gbuffer::texture_coordinate_texture], 0);
	glNamedFramebufferTexture(fbo, GL_COLOR_ATTACHMENT2, gbuf.msaa_ids[gbuffer::depth_gradient_texture], 0);
	glNamedFramebufferTexture(fbo, GL_COLOR_ATTACHMENT3, gbuf.msaa_ids[gbuffer::uv_gradient_texture], 0);
	glNamedFramebufferTexture(fbo, GL_COLOR_ATTACHMENT4, gbuf.msaa_ids[gbuffer::material_id_texture], 0);
	status = glCheckNamedFramebufferStatus(fbo, GL_DRAW_FRAMEBUFFER);
	fbo = gbuf.fbo_ids[gbuffer::sky_draw_fbo];
	glNamedFramebufferTexture(fbo, GL_DEPTH_ATTACHMENT, gbuf.msaa_ids[gbuffer::depth_texture], 0);
	glNamedFramebufferTexture(fbo, GL_COLOR_ATTACHMENT0, gbuf.msaa_ids[gbuffer::pre_resolve_texture], 0);
	status = glCheckNamedFramebufferStatus(fbo, GL_DRAW_FRAMEBUFFER);
	fbo = gbuf.fbo_ids[gbuffer::resolve_fbo];
	glNamedFramebufferTexture(fbo, GL_COLOR_ATTACHMENT0, gbuf.tex_ids[gbuffer::post_resolve_texture], 0);
	status = glCheckNamedFramebufferStatus(fbo, GL_DRAW_FRAMEBUFFER);
	err = glGetError();	
}

void terminate_gbuffer() {
	glDeleteTextures(gbuffer::msaa_texture_count, gbuf.msaa_ids);
	glDeleteTextures(gbuffer::regular_texture_count, gbuf.tex_ids);
	glDeleteFramebuffers(gbuffer::fbo_count, gbuf.fbo_ids);
}

static float4x4 g_projection;
static float4x4 g_view;

void bin_lights_and_decals_into_clusters() {}

void render_sun_and_spotlight_shadows() {}

int scene_width = 21;
int scene_height = 21;

char scene[] =
"#####################"
"#                   #"
"# #B# #B#B# #B# #B#B#"
"# BBB B     BBB B   #"
"# #B# # # # #B# # # #"
"# BBB   B   BBB   B #"
"# #B#B#B# # #B# #B# #"
"#BBBBBBB    BBBBBBB #"
"# #B#B#B# # #B#B#B# #"
"#                   #"
"# # # # # # # # # # #"
"#                   #"
"# #B# # #B# #B# #B# #"
"# BBB B     BBB B   #"
"# # # # # # # # # # #"
"#                   #"
"# # # # # # # # # # #"
"#                   #"
"# # # # # # # # # # #"
"#                   #"
"#####################"
;

void render_scene_to_gbuffer() {
	GLenum err{GL_NO_ERROR};
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glDepthMask(GL_TRUE);
	glDepthRange(0.0, 1.0);
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, gbuf.fbo_ids[gbuffer::geometry_draw_fbo]);
	GLenum draw_buffers[] = {
		GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3, GL_COLOR_ATTACHMENT4,
	};
	glDrawBuffers(5, draw_buffers);
	glClearBufferfi(GL_DEPTH_STENCIL, 0, 1.0f, 0x00);
	err = glGetError();
	uint32_t clear0[4]{};
	uint32_t clear4[4]{};
	glClearBufferuiv(GL_COLOR, 0, clear0);
	glClearBufferfv(GL_COLOR, 1, float4(0.0f, 0.0f, 0.0f, 0.0f).ptr());
	glClearBufferfv(GL_COLOR, 2, float4(0.0f, 0.0f, 0.0f, 0.0f).ptr());
	glClearBufferfv(GL_COLOR, 3, float4(0.0f, 0.0f, 0.0f, 0.0f).ptr());
	glClearBufferuiv(GL_COLOR, 4, clear4);
	err = glGetError();

	// draw the block instances
	glUseProgram(shaders.solid_prg);
	glUniformMatrix4fv(glGetUniformLocation(shaders.solid_prg, "lv_ProjectionMatrix"), 1, GL_FALSE, g_projection.Transposed().ptr());
	glUniformMatrix4fv(glGetUniformLocation(shaders.solid_prg, "lv_ViewMatrix"), 1, GL_FALSE, g_view.Transposed().ptr());
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, cube.vertices);
	glVertexArrayElementBuffer(g_vao, cube.indices);
	float3 base(-scene_width / 2.0f, 0.0f, -scene_height / 2.0f);
	struct Instance {
		float4x4 world;
		uint32_t material_id; uint32_t _pad0[3];
	};
	std::vector<Instance> instances;
	instances.reserve(scene_height * scene_width);
	for (size_t row = 0; row < scene_height; ++row) {
		for (size_t col = 0; col < scene_width; ++col) {
			char cell = scene[row * scene_width + col];
			if (cell == ' ')
				continue;
			float3 offset(static_cast<float>(col), 0.0f, static_cast<float>(row));
			float4x4 world = float4x4::Translate(2.0f * (offset + base));
			uint32_t material_id = cell == 'B' ? 3 : 2;
			instances.push_back({ world.Transposed(), material_id });
		}
	}
	GLuint instance_buffer{};
	glCreateBuffers(1, &instance_buffer);
	glNamedBufferStorage(instance_buffer, sizeof(Instance) * instances.size(), instances.data(), 0);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, instance_buffer);

	glDrawElementsInstanced(GL_TRIANGLES, cube.index_count, GL_UNSIGNED_SHORT, nullptr, static_cast<GLsizei>(instances.size()));

	glDeleteBuffers(1, &instance_buffer);

	// draw the "terrain"
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, terrain.vertices);
	glVertexArrayElementBuffer(g_vao, terrain.indices);
	instances.resize(1);
	float4x4 world = float4x4::Translate(0.0f, -1.0f, 0.0f) * float4x4::UniformScale(scene_width - 1.0f);
	instances[0].world = world.Transposed();
	instances[0].material_id = 1;
	glCreateBuffers(1, &instance_buffer);
	glNamedBufferStorage(instance_buffer, sizeof(Instance) * instances.size(), instances.data(), 0);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, instance_buffer);

	glDrawElementsInstanced(GL_TRIANGLES, terrain.index_count, GL_UNSIGNED_SHORT, nullptr, static_cast<GLsizei>(instances.size()));

	glDeleteBuffers(1, &instance_buffer);
}

void process_shading_msaa() {
	assert(g_width % 8 == 0 && g_height % 8 == 0);
	GLenum err{GL_NO_ERROR};
	err = glGetError();
	enum { k_edge_buffer = 0, k_interior_buffer = 1 };
	GLuint bufs[2]{};
	glCreateBuffers(2, bufs);
	GLintptr offsets[] = {
		0, 0,
	};
	int tile_count = g_width / 8 * g_height / 8;
	GLsizeiptr sizes[] = {
		3 * sizeof(uint32_t) + tile_count * sizeof(uint32_t),
		3 * sizeof(uint32_t) + tile_count * sizeof(uint32_t),
	};
	for (size_t i = 0; i < 2; ++i) {
		glNamedBufferStorage(bufs[i], sizes[i], nullptr, GL_DYNAMIC_STORAGE_BIT);
		uint32_t initial_counts[] = { 1, 1, 1 };
		glNamedBufferSubData(bufs[i], 0, sizeof(initial_counts), initial_counts);
	}

	// detect edge pixels, classify 8x8 tiles
	{
		GLuint prg = shaders.edge_classify_prg;
		glUseProgram(prg);
		GLuint src = gbuf.msaa_ids[gbuffer::material_id_texture];
		glBindImageTexture(0, src, 0, GL_FALSE, 0, GL_READ_ONLY, GL_R8UI);
		glUniform1i(glGetUniformLocation(prg, "lv_MaterialImage"), 0);

		glBindBuffersRange(GL_SHADER_STORAGE_BUFFER, 0, 2, bufs, offsets, sizes);
		glDispatchCompute(g_width / 8, g_height / 8, 1);
	}

	// render sky
	{
		glDisable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);
		glDepthMask(GL_FALSE);
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, gbuf.fbo_ids[gbuffer::sky_draw_fbo]);
		GLenum draw_buffers[] = { GL_COLOR_ATTACHMENT0, };
		glDrawBuffers(1, draw_buffers);
		glClearBufferfv(GL_COLOR, 0, float4(0.0f, 0.0f, 0.0f, 0.0f).ptr());
		glUseProgram(shaders.skybox_prg);
		glUniformMatrix4fv(glGetUniformLocation(shaders.skybox_prg, "lv_ProjectionMatrix"), 1, GL_FALSE, g_projection.Transposed().ptr());
		glUniformMatrix4fv(glGetUniformLocation(shaders.skybox_prg, "lv_ViewMatrix"), 1, GL_FALSE, g_view.Transposed().ptr());
		glBindTextureUnit(0, clouds.tex_id);
		glUniform1i(glGetUniformLocation(shaders.skybox_prg, "lv_Cubemap"), 0);

		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, skybox_data.vertices);
		glVertexArrayElementBuffer(g_vao, skybox_data.indices);
		glDrawElements(GL_TRIANGLES, skybox_data.index_count, GL_UNSIGNED_SHORT, nullptr);
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, screen_fbo);
	}

	// common resource binds for tile shading
	GLuint srcs[] = {
		gbuf.msaa_ids[gbuffer::tangent_frame_texture],
		gbuf.msaa_ids[gbuffer::texture_coordinate_texture],
		gbuf.msaa_ids[gbuffer::depth_gradient_texture],
		gbuf.msaa_ids[gbuffer::uv_gradient_texture],
		gbuf.msaa_ids[gbuffer::material_id_texture],
	};
	GLenum fmts[] = {
		GL_RGB10_A2UI,
		GL_RG16F,
		GL_RG16F,
		GL_RGBA16F,
		GL_R8UI,
	};

	char const* src_names[] = {
		"lv_TangentFrameImage",
		"lv_TextureCoordinateImage",
		"lv_DepthGradientImage",
		"lv_UvGradientImage",
		"lv_MaterialImage",
	};
	GLuint dst = gbuf.msaa_ids[gbuffer::pre_resolve_texture];
	GLuint diffuse = dbg_tex.uv;

	// run deferred compute for non-edge tiles
	{
		GLuint prg = shaders.shade_interior_prg;
		glUseProgram(prg);

		for (int i = 0; i < _countof(srcs); ++i) {
			GLint slot = i;
			glBindImageTexture(slot, srcs[i], 0, GL_FALSE, 0, GL_READ_ONLY, fmts[i]);
			glUniform1i(glGetUniformLocation(prg, src_names[i]), slot);
		}
		GLint dst_slot = _countof(srcs);
		glBindImageTexture(dst_slot, dst, 0, GL_FALSE, 0, GL_READ_WRITE, GL_RGBA8);
		glUniform1i(glGetUniformLocation(prg, "lv_TargetImage"), dst_slot);
		
		GLint tex_slot = 0;
		glBindTextureUnit(tex_slot, diffuse);
		glUniform1i(glGetUniformLocation(prg, "lv_DiffuseTexture"), tex_slot);

		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, bufs[k_interior_buffer]);
		glBindBuffer(GL_DISPATCH_INDIRECT_BUFFER, bufs[k_interior_buffer]);
		glDispatchComputeIndirect(0);
	}

	// run deferred compute for edge tiles
	{
		GLuint prg = shaders.shade_edge_prg;
		glUseProgram(prg);

		for (int i = 0; i < _countof(srcs); ++i) {
			GLint slot = i;
			glBindImageTexture(slot, srcs[i], 0, GL_FALSE, 0, GL_READ_ONLY, fmts[i]);
			glUniform1i(glGetUniformLocation(prg, src_names[i]), slot);
		}
		GLint dst_slot = _countof(srcs);
		glBindImageTexture(dst_slot, dst, 0, GL_FALSE, 0, GL_READ_WRITE, GL_RGBA8);
		glUniform1i(glGetUniformLocation(prg, "lv_TargetImage"), dst_slot);

		glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, bufs[k_edge_buffer]);
		glBindBuffer(GL_DISPATCH_INDIRECT_BUFFER, bufs[k_edge_buffer]);
		glDispatchComputeIndirect(0);
	}

	// resolve into non-msaa
	{
		if (debug_perspective == 1 || debug_perspective == 5) {
			size_t selector = debug_perspective == 5;
			GLuint prgs[] = {
				shaders.debug_resolve_rgb10_a2ui_prg, shaders.debug_resolve_r8ui_prg,
			};
			GLuint srcs[] = {
				gbuf.msaa_ids[gbuffer::tangent_frame_texture], gbuf.msaa_ids[gbuffer::material_id_texture],
			};
			GLenum fmts[] = {
				GL_RGB10_A2UI, GL_R8UI,
			};
			GLuint dst = gbuf.tex_ids[gbuffer::post_resolve_texture];
			glUseProgram(prgs[selector]);
			glBindImageTexture(0, srcs[selector], 0, GL_FALSE, 0, GL_READ_ONLY, fmts[selector]);
			glBindImageTexture(1, dst, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA8);
			glUniform1i(glGetUniformLocation(prgs[selector], "lv_SourceImage"), 0);
			glUniform1i(glGetUniformLocation(prgs[selector], "lv_TargetImage"), 1);

			glDispatchCompute(g_width / 8, g_height / 8, 1);
			err = glGetError();
		}
		else {
			GLenum from_attachment = GL_COLOR_ATTACHMENT0;
			GLuint from_fbo = gbuf.fbo_ids[gbuffer::sky_draw_fbo];
			if (debug_perspective > 0) {
				from_attachment = GL_COLOR_ATTACHMENT0 + (debug_perspective - 1);
				from_fbo = gbuf.fbo_ids[gbuffer::geometry_draw_fbo];
			}
			GLenum to_attachment = GL_COLOR_ATTACHMENT0;
			GLuint to_fbo = gbuf.fbo_ids[gbuffer::resolve_fbo];
			glNamedFramebufferReadBuffer(from_fbo, from_attachment);
			glNamedFramebufferDrawBuffers(to_fbo, 1, &to_attachment);
			glBlitNamedFramebuffer(from_fbo, to_fbo,
				0, 0, g_width, g_height, 0, 0, g_width, g_height, GL_COLOR_BUFFER_BIT, GL_NEAREST);
			glBindFramebuffer(GL_DRAW_FRAMEBUFFER, screen_fbo);
			err = glGetError();
		}
	}
	glDeleteBuffers(2, bufs);
}

void post_proc() {}

void present() {
	glNamedFramebufferReadBuffer(gbuf.fbo_ids[gbuffer::resolve_fbo], GL_COLOR_ATTACHMENT0);
	glBlitNamedFramebuffer(gbuf.fbo_ids[gbuffer::resolve_fbo], screen_fbo,
		0, 0, g_width, g_height, 0, 0, g_width, g_height, GL_COLOR_BUFFER_BIT, GL_NEAREST);
	//glClearBufferfv(GL_COLOR, 0, float4(0.1f, 0.2f, 0.3f, 1.0f).ptr());
	glfwSwapBuffers(g_window);
}

void load_skybox() {
	GLenum err{GL_NO_ERROR};
	// char const* suffixes[] = { "rt", "lf", "up", "dn", "ft", "bk" };
	char const* suffixes[] = { "rt", "lf", "up", "dn", "bk", "ft" };
	char const* files[] = { "right.jpg", "left.jpg", "top.jpg", "bottom.jpg", "back.jpg", "front.jpg" };
	glCreateTextures(GL_TEXTURE_CUBE_MAP, 1, &clouds.tex_id);
	err = glGetError();
	for (size_t i = 0; i < 6; ++i) {
		std::string filename = fs_root + "\\assets\\skyboxes\\learnopengl.com\\" + files[i];
		int w{}, h{}, comp{};
		void* data = stbi_load(filename.c_str(), &w, &h, &comp, 3);
		if (i == 0) {
			glTextureStorage2D(clouds.tex_id, 1, GL_RGB8, w, h);
			err = glGetError();
		}
		glTextureSubImage3D(clouds.tex_id, 0, 0, 0, static_cast<GLint>(i), w, h, 1, GL_RGB, GL_UNSIGNED_BYTE, data);
		err = glGetError();
		stbi_image_free(data);
	}
}

void load_misc_textures() {
	{
		int w, h, comp;
		uint8_t* data = stbi_load((fs_root + "\\assets\\UvIMG2.jpg").c_str(), &w, &h, &comp, 3);
		int mip_count = 0;
		for (int dim = (std::max)(w, h); dim >= 1; dim /= 2) {
			++mip_count;
		}
		auto& tex = dbg_tex.uv;
		glCreateTextures(GL_TEXTURE_2D, 1, &tex);
		glTextureStorage2D(tex, mip_count, GL_RGB8, w, h);
		glTextureSubImage2D(tex, 0, 0, 0, w, h, GL_RGB, GL_UNSIGNED_BYTE, data);
		glGenerateTextureMipmap(tex);
		stbi_image_free(data);
	}
}

int main() {
	// Doing bind-less texturing for deferred shading and decals, but with texture arrays or something.
	// https://mynameismjp.wordpress.com/2016/03/25/bindless-texturing-for-deferred-rendering-and-decals/
	fs_init();
	vid_init();
	shader_init();
	init_gbuffer();
	load_skybox();
	load_misc_textures();
	geometry_init();
	while (g_keep_running) {
		glfwPollEvents();

		char const* titles[] = {
			"untitled - resolved",
			"untitled - tangent frame",
			"untitled - texture coordinates",
			"untitled - depth gradients",
			"untitled - uv gradients",
			"untitled - material id",
		};
		glfwSetWindowTitle(g_window, titles[debug_perspective]);

		double t = glfwGetTime();
		float3 base_eye = 30.0f * (1.3f * float3::unitY + 1.0f * float3::unitZ);
		float3x3 eye_rotate = float3x3::RotateY(static_cast<float>(0.2 + 2.0 * t * 0.1));
		g_projection = float4x4::OpenGLPerspProjRH(0.1f, 100.0f, 0.1f * g_aspect, 0.1f);
		g_view = float4x4::LookAt(eye_rotate * base_eye, float3::zero, -float3::unitZ, float3::unitY, float3::unitY).Inverted();

		bin_lights_and_decals_into_clusters();
		render_sun_and_spotlight_shadows();
		render_scene_to_gbuffer();
		process_shading_msaa();
		post_proc();
		present();

		if (glfwWindowShouldClose(g_window)) {
			g_keep_running = false;
		}
	}
	terminate_gbuffer();
	vid_terminate();
	return 0;
}